#include "stdafx.h"
#include "inc/main.h"

#include "script.h"

Ped GetPlayerPed()
{
	if (PLAYER::DOES_MAIN_PLAYER_EXIST())
	{
		return PLAYER::GET_PLAYER_PED(PLAYER::GET_PLAYER_ID());
	}

	return NULL;
}

void main()
{
	bool enabled = GetPrivateProfileInt(L"SETTINGS", L"ENABLED_BY_DEFAULT", 1, L".\\PowerfulBullets.ini") != 0;
	int toggleKey = GetPrivateProfileInt(L"SETTINGS", L"TOGGLE_KEY", 0x4B, L".\\PowerfulBullets.ini"); //default is K
	float pedForceMulti = (float)GetPrivateProfileInt(L"SETTINGS", L"PED_FORCE_MULTIPLIER", 1000, L".\\PowerfulBullets.ini");
	float pedForceZ = (float)GetPrivateProfileInt(L"SETTINGS", L"PED_FORCE_UPWARDS", 200, L".\\PowerfulBullets.ini");
	float vehForceMulti = (float)GetPrivateProfileInt(L"SETTINGS", L"VEHICLE_FORCE_MULTIPLIER", 4000, L".\\PowerfulBullets.ini");
	float vehForceZ = (float)GetPrivateProfileInt(L"SETTINGS", L"VEHICLE_FORCE_UPWARDS", 400, L".\\PowerfulBullets.ini");

	int lastGetPeds = 0, lastGetVehs = 0;
	int numPeds = 0, numVehs = 0;
	Ped worldPeds[16];
	Vehicle worldVehs[16];
	Vector3 lastImpactCoord = Vector3::zero();

	while (true)
	{
		if (!HUD::IS_PAUSE_MENU_ACTIVE() && CUTSCENE::GET_CUTSCENE_TIME_MS() == 0)
		{
			if (IsKeyJustUp(toggleKey))
			{
				enabled = !enabled;

				const char *printStr = "Powerful Bullets disabled";
				if (enabled) printStr = "Powerful Bullets enabled";

				HUD::PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", printStr, 1000, 1);
			}

			if (enabled)
			{
				Ped plrPed = GetPlayerPed();

				if (plrPed != NULL)
				{
					int gameTime = MISC::GET_GAME_TIMER();

					if (gameTime - lastGetPeds >= 1000)
					{
						numPeds = worldGetAllPeds(worldPeds, 16);
						lastGetPeds = gameTime;
					}

					if (gameTime - lastGetVehs >= 1500)
					{
						numVehs = worldGetAllVehicles(worldVehs, 16);
						lastGetVehs = gameTime;
					}

					Vector3 impactCoord;
					WEAPON::GET_PED_LAST_WEAPON_IMPACT_COORD(plrPed, &impactCoord);

					if (impactCoord != lastImpactCoord)
					{
						lastImpactCoord = impactCoord;

						Vector3 plrPedCoord = PED::GET_PED_COORDS(plrPed);
						Vector3 entityCoord, direction;

						if (numPeds > 0)
						{
							Ped ped;

							for (int i = 0; i < numPeds; i++)
							{
								ped = worldPeds[i];

								if (ped != NULL && PED::DOES_PED_EXIST(ped) && ped != plrPed)
								{
									entityCoord = PED::GET_PED_COORDS(ped);

									if (MISC::GET_DISTANCE_BETWEEN_COORDS(entityCoord.x, entityCoord.y, entityCoord.z, impactCoord.x, impactCoord.y, impactCoord.z, true) <= 1.0f)
									{
										direction = (entityCoord - plrPedCoord).normalize() * pedForceMulti;
										direction.z = pedForceZ;

										PED::SET_PED_TO_RAGDOLL(ped, 3000, 3000, 0, false, false, true, -1056964608);
										PED::APPLY_FORCE_TO_PED(ped, 1, direction.x, direction.y, direction.z, direction.x / 3.0f, direction.y / 3.0f, direction.z / 3.0f, (int)ePedBone::ROOT, false, false, false);
									}
								}
							}
						}

						if (numVehs > 0)
						{
							Vehicle veh;

							for (int i = 0; i < numVehs; i++)
							{
								veh = worldVehs[i];

								if (veh != NULL && VEHICLE::DOES_VEHICLE_EXIST(veh))
								{
									entityCoord = VEHICLE::GET_VEHICLE_COORDS(veh);

									if (MISC::GET_DISTANCE_BETWEEN_COORDS(entityCoord.x, entityCoord.y, entityCoord.z, impactCoord.x, impactCoord.y, impactCoord.z, true) <= 3.0f)
									{
										direction = (entityCoord - plrPedCoord).normalize() * vehForceMulti;
										direction.z = vehForceZ;

										VEHICLE::APPLY_FORCE_TO_VEHICLE(worldVehs[i], 2, direction.x, direction.y, direction.z, direction.x / 3.0f, direction.y / 3.0f, direction.z / 3.0f, 0, false, false, true);
									}
								}
							}
						}
					}
				}
			}
		}

		scriptWait(0);
	}
}

void ScriptMain()
{
	srand(GetTickCount());
	main();
}
